# Backbase - Festival Component

 

### Prerequisites

To install required libraries, Node Package Manager(NPM) needed which comes with Nodejs.


### Installing

In the project root directory by typing below (in the terminal/console), the required libraries can be installed. 

```
npm install
```
    

## Author

 **Serkan Demirel** - *Festival Component*  

 [Git Repo](https://bitbucket.org/serdem420/backbase_festival)  

 
## How to run

After installing the required libraries by following the steps in the "Installing" section you can run the Index.html file directly.