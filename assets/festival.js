
//map initialize function
function initMap (mapContainer,lat,lng){

    var uluru  = {lat: lat, lng: lng};
    var map    = new google.maps.Map (mapContainer, {
        center:  new google.maps.LatLng(uluru.lat, uluru.lng),
        zoom  : 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var marker = new google.maps.Marker ({
        position: uluru,
        map     : map
    });

    $(mapContainer).addClass("loaded")
}

//since the api returns single field for lat and lng as string, here are two handlebars extraction helpers
Handlebars.registerHelper('lat', function(position) {
    var positionArray = position.split(" ");
    return positionArray[0];
});
Handlebars.registerHelper('lng', function(position) {
    var positionArray = position.split(" ");
    return positionArray[1];
});
// END since the rest api returns single lat and lng here are two handlebars extraction helpers


$ (document).ready (function (){

    var source               = $ ("#festival-template").html ();
    var template             = Handlebars.compile (source);
    var festivalListEndpoint = "http://citysdk.dmci.hva.nl/CitySDK/events/search?category=festival";
    $.getJSON (festivalListEndpoint, prepareView, fail);

    //api request success handler
    function prepareView (data){
        var context = data.event;
        var html    = template (context);
        $ ("#app").append (html);

        $(".panel-heading").each(function (index){

        });
    }

    //api request error handler
    function fail (jqxhr, textStatus, error){
        console.log ("Request Failed: " + err);
    }



    //on click event for festival names
    $ ("body").on ("click", "festival .panel-heading", function (e){

        //toggle panel
        var $panelBody = $(this).parent().find(".panel-body");
        $panelBody.toggleClass("hide");

        //store the status of map load: if loaded already, return
        var loadStatus = $(this).hasClass("loaded");
        if(loadStatus) return true;

        //extract position
        var lat = $(this).data("lat");
        var lng = $(this).data("lng");

        //get map container
        var map = $(this).parent().find("#map").get(0);

        //initialize map
        initMap (map,lat,lng);


    });

});